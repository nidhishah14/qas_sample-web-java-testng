package tests.web.transactions;

import java.util.regex.Pattern;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.step.WsStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;

public class VerifyCreditedAmount extends WebDriverTestCase {
   private QAFExtendedWebDriver driver;
   private String baseUrl = "https://qas.qmetry.com/bank";
   private boolean acceptNextAlert = true;
   private StringBuffer verificationErrors = new StringBuffer();
   String username = "Bob";
   String password = "Bob";
   String amountToBeCredited;

   @BeforeClass(alwaysRun = true)
   public void setUp() throws Exception {
      driver = getDriver();
   }

   /**
    * Test method to verify successful credit transaction
    * 
    * @param data : data of transactionData.csv file
    * @throws Exception
    */
   @QAFDataProvider(dataFile = "resources/testdata/transactionData.csv")
   @Test
   public void VerifyCreditedAmount(Map<String, String> data) throws Exception {

      // Get amount to be credited from datafile
      amountToBeCredited = data.get("credit");

      // Navigate to URL
      driver.get(baseUrl);

      // Maximize window
      driver.manage().window().maximize();

      // Login
      CommonStep.clear("text.txtusername");
      CommonStep.sendKeys(username, "text.txtusername");
      CommonStep.clear("password.txtpassword");
      CommonStep.sendKeys(password, "password.txtpassword");
      driver.findElement("button.btnlogin").waitForEnabled();
      CommonStep.click("button.btnlogin");

      // Assert - Successfull login
      CommonStep.assertVisible("button.button");

      // Get the value of current balance
      CommonStep.getText("strong.usergloberank");
      CommonStep.storeLastStepResultInto("currentBalance");

      // Credit the amount
      CommonStep.waitForEnabled("number.enteramountforcredit", 3);
      CommonStep.clear("number.enteramountforcredit");
      CommonStep.sendKeys(amountToBeCredited, "number.enteramountforcredit");
      driver.findElement("button.button11").waitForEnabled();
      CommonStep.click("button.button11");

      // Assert - Amount have been credited
      CommonStep.assertText("strong.usergloberank", updatedValue(amountToBeCredited));

      // Logout
      driver.findElement("button.button").waitForEnabled();
      CommonStep.click("button.button");

      // Assert - Successful logout
      CommonStep.assertVisible("button.btnlogin");
   }

   /**
    * Update the value of current balance
    * 
    * @param amountToBeCredited : Amount to be added into current balance
    * @return
    */
   public String updatedValue(String amountToBeCredited) {
      int currentBalance = Integer
            .parseInt(ConfigurationManager.getBundle().getProperty("currentBalance").toString().split(" ")[1]);
      currentBalance = currentBalance + Integer.parseInt(amountToBeCredited);
      return "$ " + currentBalance;
   }

   @AfterClass(alwaysRun = true)
   public void tearDown() throws Exception {
      String verificationErrorString = verificationErrors.toString();
      if (!"".equals(verificationErrorString)) {
         fail(verificationErrorString);
      }
   }

   private boolean isElementPresent(By by) {
      try {
         driver.findElement(by);
         return true;
      } catch (NoSuchElementException e) {
         return false;
      }
   }

   private boolean isAlertPresent() {
      try {
         driver.switchTo().alert();
         return true;
      } catch (NoAlertPresentException e) {
         return false;
      }
   }

   private String closeAlertAndGetItsText() {
      try {
         Alert alert = driver.switchTo().alert();
         String alertText = alert.getText();
         if (acceptNextAlert) {
            alert.accept();
         } else {
            alert.dismiss();
         }
         return alertText;
      } finally {
         acceptNextAlert = true;
      }
   }
}
